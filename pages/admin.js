import React from 'react';
import Page from '../src/components/Page';
import Title from '../src/components/Title';
import Articles from '../src/components/Admin/Articles';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import actions from '../utils/redux/actions';
import Categories from '../src/components/Admin/Categories';
import Users from '../src/components/Admin/Users';
import { Box, Paper, Tabs, Tab, AppBar, Typography } from '@material-ui/core';
import SettingsRoundedIcon from '@material-ui/icons/SettingsRounded';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { withSnackbar } from '../src/components/SnackbarProvider';
import { makeStyles, useTheme } from '@material-ui/core/styles';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};


function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: 500,
  },
}));

const Admin = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(1);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = index => {
    setValue(index);
  };

  return (
    <Page>
      <Title label="Administração do Sistema" sub="Cadastros e Cia" Icon={SettingsRoundedIcon}/>

      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Artigos" {...a11yProps(0)} />
          <Tab label="Categorias" {...a11yProps(1)} />
          <Tab label="Usuários" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <Articles/>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <Categories/>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
          <Users/>
        </TabPanel>
      </SwipeableViews>
      
    </Page>
  );
}

const mapStateToProps = (state) => {
  return {
    loading: state.authentication.loading
  };
}


export default compose(connect(mapStateToProps, actions), withSnackbar())(Admin);