import React from 'react';
import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import { initStore } from '../utils/redux';
import App from 'next/app';
import Head from 'next/head';
import CssBaseline from '@material-ui/core/CssBaseline';
import parser from 'ua-parser-js';
import { SnackbarProvider } from '../src/components/SnackbarProvider'
import mediaQuery from 'css-mediaquery';
import "easymde/dist/easymde.min.css";

class Brainbase extends App {

  async getInitialProps({ctx}) {
    console.log('req');
    const deviceType = parser(ctx.req.headers['user-agent']).device.type || 'desktop';
    console.log(deviceType);
    const ssrMatchMedia = query => ({
      matches: mediaQuery.match(query, {
        // The estimated CSS width of the browser.
        width: deviceType === 'mobile' ? '0px' : '1024px',
      }),
    });

    return {ssrMatchMedia: ssrMatchMedia};
  }

  componentDidMount() {
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles && jssStyles.parentNode)
      jssStyles.parentNode.removeChild(jssStyles)
  }

  render() {
    const { Component, pageProps, store, ssrMatchMedia } = this.props;

    return (
      <React.Fragment>
        <Head>
          <title>Brainbase</title>
        </Head>
        <Provider store={store}>
          {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
          <CssBaseline />
          <SnackbarProvider>
            <Component {...pageProps} />
          </SnackbarProvider>
            
        </Provider>
      </React.Fragment>
    );
  }
}


//);

Brainbase.getInitialProps = async (appContext) => {
let deviceType = 'desktop';

if (appContext.ctx.req && parser(appContext.ctx.req.headers['user-agent']).device.type){
  deviceType = parser(appContext.ctx.req.headers['user-agent']).device.type;
}

const ssrMatchMedia = query => ({
  matches: mediaQuery.match(query, {
    // The estimated CSS width of the browser.
    width: deviceType === 'mobile' ? '0px' : '1024px',
  }),
});
const appProps = await App.getInitialProps(appContext);
return {...appProps, ssrMatchMedia: ssrMatchMedia};
}

export default withRedux(initStore, { debug: true })(Brainbase);
