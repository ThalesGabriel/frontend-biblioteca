import React, { useEffect, useState } from 'react'
import Page from '../src/components/Page'
import Title from '../src/components/Title'
import { Box, Typography, Paper, Grid} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import axios from 'axios';
import NoteIcon from '@material-ui/icons/Note';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import DescriptionIcon from '@material-ui/icons/Description';
import PersonIcon from '@material-ui/icons/Person';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    display: 'flex'
  },
  icon: {
    fontSize: 60,
  },
  red: {
    color: 'red',
  },
  blue: {
    color: 'blue',
  },
  green: {
    color: 'green',
  },
}));

export default function Index() {
  const [ estatisticas, setEstatisticas ] = useState({})
  const classes = useStyles();
  useEffect(() => {
    axios.get("http://localhost:3001/stats")
      .then(res => setEstatisticas(res.data))
  }, [])

  return (
    <Page>
      <Title label="Dashboard" sub="Base de conhecimento" Icon={HomeRoundedIcon}/>
      <Grid container spacing={3}>
        <Grid item md={3} sm={6} xs={12}>
          <Paper className={classes.paper}>
            <FileCopyIcon className={[classes.icon ]}/>
            <Box style={{flex: 1, textAlign: 'right'}}>
              <Typography component="h4" variant="h4">Categorias</Typography>
              <Typography component="h4" variant="h4">{estatisticas.categories || 0}</Typography>
            </Box>
          </Paper>
        </Grid>
        <Grid item md={3} sm={6} xs={12}>
          <Paper className={classes.paper}>
            <DescriptionIcon className={[classes.icon ]}/>
            <Box style={{flex: 1, textAlign: 'right'}}>
              <Typography component="h4" variant="h4">Artigos</Typography>
              <Typography component="h4" variant="h4">{estatisticas.articles || 0}</Typography>
            </Box>
          </Paper>
        </Grid>
        <Grid item md={3} sm={6} xs={12}>
          <Paper className={classes.paper}>
            <PersonIcon className={[classes.icon ]}/>
            <Box style={{flex: 1, textAlign: 'right'}}>
              <Typography component="h4" variant="h4">Usuários</Typography>
              <Typography component="h4" variant="h4">{estatisticas.users || 0}</Typography>
            </Box>  
          </Paper>
        </Grid>
      </Grid>
    </Page>
  );
}