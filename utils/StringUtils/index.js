var StringUtils = (function(){

  return {
    getColumnsToTableRecord: function (columns) { 
      const columnsList = [];
      for (let i = 0; i < columns.length; i++) {
        columnsList.push({ title: this.capitalize(columns[i]), field: columns[i] })
      }

      return columnsList;
    },

    changeColumnName: function (columns, newColumns) { 
      for (let i = 0; i < newColumns.length; i++) {
        let tilte = columns[i]["title"];
        if(newColumns[i] != null) {
          title = newColumns[i]
          columns[i]["title"] = title
        }
      }
      return columns;
    },

    deleteColumn: function (columns, removeColumns) { 
      for (let i = 0; i < removeColumns.length; i++) {
        if(removeColumns[i] != null && columns[i]["title"] == removeColumns[i]) {
          columns.splice(i,1);
        }
      }
      return columns;
    },

    capitalize: function (s) {
      if (typeof s !== 'string') return ''
      return s.charAt(0).toUpperCase() + s.slice(1)
    },
     
  }
 
 }())

module.exports = StringUtils;