import {
  REGISTER_REQUESTED,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  UPDATE_REQUESTED,
  UPDATE_SUCCESS,
  UPDATE_FAIL,
  REMOVE_REQUESTED,
  REMOVE_SUCCESS,
  REMOVE_FAIL,
  USERS_REQUESTED,
  USERS_SUCCESS,
  USERS_FAIL
} from '../types';

const initialState = {
  loading: false,
  success: false,
  submitted: false,
  user: {},
  users: [],
  columns: [],
  error: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_REQUESTED:
      return Object.assign({}, state, { loading: true });
    case REGISTER_SUCCESS:
      return Object.assign({}, state, { loading: false, error: null, success: true, submitted: true,  user: action.user });
    case REGISTER_FAIL:
      return Object.assign({}, state, { loading: false, error: action.error, success: false, submitted: true,  user: action.user });
    case UPDATE_REQUESTED:
      return Object.assign({}, state, { loading: true, });
    case UPDATE_SUCCESS:
      return Object.assign({}, state, { loading: false, success: action.success });
    case UPDATE_FAIL:
      return Object.assign({}, state, { loading: false, error: action.error });
    case REMOVE_REQUESTED:
      return Object.assign({}, state, { loading: true });
    case REMOVE_SUCCESS:
      return Object.assign({}, state, { loading: false, success: action.success, error: null });
    case REMOVE_FAIL:
      return Object.assign({}, state, { loading: false, success: false, error: action.error });
    case USERS_REQUESTED:
      return Object.assign({}, state, { loading: true });
    case USERS_SUCCESS:
      return Object.assign({}, state, { loading: false, users: action.users, columns: action.columns, success: true, error: null});
    case USERS_FAIL:
      return Object.assign({}, state, { loading: false, success: false, error: action.error, users: [], columns: [] });
    default:
      return state;
  }
};