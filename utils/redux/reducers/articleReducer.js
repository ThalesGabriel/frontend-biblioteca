import {
  REGISTER_REQUESTED,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  UPDATE_REQUESTED,
  UPDATE_SUCCESS,
  UPDATE_FAIL,
  REMOVE_REQUESTED,
  REMOVE_SUCCESS,
  REMOVE_FAIL,
  CATEGORIES_REQUESTED,
  CATEGORIES_SUCCESS,
  CATEGORIES_FAIL,
  USERS_REQUESTED,
  USERS_SUCCESS,
  USERS_FAIL,
  ARTICLES_REQUESTED,
  ARTICLES_SUCCESS,
  ARTICLES_FAIL
} from '../types';

const initialState = {
  loading: false,
  success: false,
  submitted: false,
  articles: [],
  article: {},
  columns: [],
  error: null,
  time: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_REQUESTED:
      return Object.assign({}, state, { loading: true });
    case REGISTER_SUCCESS:
      return Object.assign({}, state, { loading: false, error: null, success: true, submitted: true,  article: action.article, time: action.time });
    case REGISTER_FAIL:
      return Object.assign({}, state, { loading: false, error: action.error, success: false, submitted: true,  article: action.article, time: action.time });
    case UPDATE_REQUESTED:
      return Object.assign({}, state, { loading: true, });
    case UPDATE_SUCCESS:
      return Object.assign({}, state, { loading: false, success: action.success });
    case UPDATE_FAIL:
      return Object.assign({}, state, { loading: false, error: action.error });
    case REMOVE_REQUESTED:
      return Object.assign({}, state, { loading: true });
    case REMOVE_SUCCESS:
      return Object.assign({}, state, { loading: false, success: action.success, error: null });
    case REMOVE_FAIL:
      return Object.assign({}, state, { loading: false, success: false, error: action.error });
    case ARTICLES_REQUESTED:
      return Object.assign({}, state, { loading: true });
    case ARTICLES_SUCCESS:
      return Object.assign({}, state, { loading: false, articles: action.articles, columns: action.columns, success: true, error: null});
    case ARTICLES_FAIL:
      return Object.assign({}, state, { loading: false, success: false, error: action.error, articles: [], columns: [] });
    default:
      return state;
  }
};