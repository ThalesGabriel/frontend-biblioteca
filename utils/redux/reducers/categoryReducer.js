import {
  REGISTER_REQUESTED,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  UPDATE_REQUESTED,
  UPDATE_SUCCESS,
  UPDATE_FAIL,
  REMOVE_REQUESTED,
  REMOVE_SUCCESS,
  REMOVE_FAIL,
  CATEGORIES_REQUESTED,
  CATEGORIES_SUCCESS,
  CATEGORIES_FAIL
} from '../types';

const initialState = {
  loading: false,
  success: false,
  submitted: false,
  category: {},
  categories: [],
  columns: [],
  error: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_REQUESTED:
      return Object.assign({}, state, { loading: true });
    case REGISTER_SUCCESS:
      return Object.assign({}, state, { loading: false, error: null, success: true, submitted: true,  category: action.category });
    case REGISTER_FAIL:
      return Object.assign({}, state, { loading: false, error: action.error, success: false, submitted: true,  category: action.category });
    case UPDATE_REQUESTED:
      return Object.assign({}, state, { loading: true, });
    case UPDATE_SUCCESS:
      return Object.assign({}, state, { loading: false, success: action.success });
    case UPDATE_FAIL:
      return Object.assign({}, state, { loading: false, error: action.error });
    case REMOVE_REQUESTED:
      return Object.assign({}, state, { loading: true });
    case REMOVE_SUCCESS:
      return Object.assign({}, state, { loading: false, success: action.success, error: null });
    case REMOVE_FAIL:
      return Object.assign({}, state, { loading: false, success: false, error: action.error });
    case CATEGORIES_REQUESTED:
      return Object.assign({}, state, { loading: true });
    case CATEGORIES_SUCCESS:
      return Object.assign({}, state, { loading: false, categories: action.categories, columns: action.columns, success: true, error: null});
    case CATEGORIES_FAIL:
      return Object.assign({}, state, { loading: false, success: false, error: action.error, categories: [], columns: [] });
    default:
      return state;
  }
};