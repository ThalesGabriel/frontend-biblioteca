import {
  REGISTER_REQUESTED,
  REGISTER_SUCCESS,
  REGISTER_FAIL
} from '../types';

const initialState = {
  loading: false,
  user: null,
  article: null,
  caregory: null,
  error: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_REQUESTED:
      return Object.assign({}, state, { loading: true });
    case REGISTER_SUCCESS:
      return Object.assign({}, state, { loading: false, user: action.user });
    case REGISTER_FAIL:
      return Object.assign({}, state, { loading: false, user: false, error: action.error });
    default:
      return state;
  }
};
