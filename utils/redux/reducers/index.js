import { combineReducers } from 'redux';
import authReducer from './authReducer';
import userReducer from './userReducer';
import categoryReducer from './categoryReducer';
import articleReducer from './articleReducer';

const rootReducer = combineReducers({
  authentication: authReducer,
  categoryReducer: categoryReducer,
  userReducer: userReducer,
  articleReducer: articleReducer
});

export default rootReducer;
