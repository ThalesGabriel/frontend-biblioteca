import Router from 'next/router';
import {
  REGISTER_REQUESTED,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  UPDATE_REQUESTED,
  UPDATE_SUCCESS,
  UPDATE_FAIL,
  REMOVE_REQUESTED,
  REMOVE_SUCCESS,
  REMOVE_FAIL,
  ARTICLES_REQUESTED,
  ARTICLES_SUCCESS,
  ARTICLES_FAIL,
} from '../types';
import { baseURL } from '../../../services/api';
import StringUtils from '../../StringUtils';
import axios from 'axios';

const getAllArticles = (returnToDataTable = false) => dispatch => {
  dispatch({type: ARTICLES_REQUESTED});

  console.log('Relação de artigos requisitada...');

  axios.get("http://localhost:3001/articles").then(response => {
    if(!returnToDataTable) dispatch({type: ARTICLES_SUCCESS, articles: response.data.data, columns: [], error: null });
    if(response.data.data.length > 0) {
      const columns = Object.keys(response.data.data[0])
      let columnsList = StringUtils.getColumnsToTableRecord(columns);
      columnsList = StringUtils.changeColumnName(columnsList, [null, "Nome", "Descrição"])
      dispatch({type: ARTICLES_SUCCESS, articles: response.data.data, columns: columnsList });
    }
  }).catch(err => {
      dispatch({type: ARTICLES_FAIL, error: err.response.data});
  })
  return Promise.resolve();
};

const registerArticle = (values) => dispatch => {
  dispatch({type: REGISTER_REQUESTED});
  console.log(values)
  console.log('Registro de artigo requisitado...');

  axios.post("http://localhost:3001/articles", values).then(response => {
    //dispatch(getAllCategories(true));
    dispatch({type: REGISTER_SUCCESS, error: null, article: values, time: new Date() });
  }).catch(err => {
    dispatch({type: REGISTER_FAIL, error: err.response.data, article: values, time: new Date()});
  }) 
  return Promise.resolve();
};

const removeArticle = (id) => dispatch => {

  dispatch({type: REMOVE_REQUESTED});

  console.log('Remoção de artigo requisitada...');

  axios.delete(`http://localhost:3001/articles/${id}`).then(response => {
    dispatch(getAllArticles(true));
    dispatch({type: REMOVE_SUCCESS, error: null});
  }).catch(err => {
    dispatch({type: REMOVE_FAIL, error: err.response.data });
  }) 
  return Promise.resolve();
};
      

const editArticle = (id, values) => dispatch => {
  dispatch({type: UPDATE_REQUESTED});

  console.log('Update de categoria requisitado...');

  axios.put(`http://localhost:3001/articles/${id}`, values).then(response => {
    dispatch(getAllArticles(true));
    dispatch({type: UPDATE_SUCCESS, success: true });
  }).catch(err => {
    dispatch({type: UPDATE_FAIL, error: err.response.data});
  })
  return Promise.resolve();
};



export default {
  registerArticle,
  getAllArticles
};