import authActions from './authActions';
import userActions from './userActions';
import categoryActions from './categoryActions';
import articleActions from './articleActions';

export default {
  ...authActions,
  ...categoryActions,
  ...userActions,
  ...articleActions
}