import Router from 'next/router';
import {
  REGISTER_REQUESTED,
  REGISTER_SUCCESS,
  REGISTER_FAIL
} from '../types';
import { baseURL } from '../../../services/api';
import axios from 'axios';

const register = (usuario) => {
  //social: 0
  return (dispatch) => {
    dispatch({type: REGISTER_REQUESTED});

    console.log('registro de usuário requisitado...');

    axios.post("http://localhost:3001/signup", usuario).then(response => {
      if (response.status == 204) {
        dispatch({type: REGISTER_SUCCESS, error: false, user: true });
      } else {
        console.log("Registro Falhou")
        dispatch({type: REGISTER_FAIL, user: null, error: {message: response.data.error}});
      }
    })
  };
};

const return_state = () => {
  //social: 0
  return (dispatch) => {
    dispatch({type: "DEFAULT"});

    console.log('Voltando ao estado original...');
  }
};

export default {
  register,
  return_state
};
