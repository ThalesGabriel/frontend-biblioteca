import Router from 'next/router';
import {
  REGISTER_REQUESTED,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  UPDATE_REQUESTED,
  UPDATE_SUCCESS,
  UPDATE_FAIL,
  REMOVE_REQUESTED,
  REMOVE_SUCCESS,
  REMOVE_FAIL,
  CATEGORIES_REQUESTED,
  CATEGORIES_SUCCESS,
  CATEGORIES_FAIL,
} from '../types';
import { baseURL } from '../../../services/api';
import StringUtils from '../../StringUtils';
import axios from 'axios';

const getAllCategories = (returnToDataTable = false) => dispatch => {
  dispatch({type: CATEGORIES_REQUESTED});

  console.log('Relação de catagorias requisitada...');

  axios.get("http://localhost:3001/categories").then(response => {
    if(!returnToDataTable) dispatch({type: CATEGORIES_SUCCESS, categories: response.data, columns: [], error: null });
    if(response.data.length > 0){
      const columns = Object.keys(response.data[0])
      let columnsList = StringUtils.getColumnsToTableRecord(columns);
      columnsList = StringUtils.changeColumnName(columnsList, [null, "Nome", null, "Caminho"])
      columnsList = StringUtils.deleteColumn(columnsList, [null, null, "ParentId", null])
      dispatch({type: CATEGORIES_SUCCESS, categories: response.data, columns: columnsList });
    }
  }).catch(err => {
    dispatch({type: CATEGORIES_FAIL, error: err.response.data});
  })
  return Promise.resolve();
}

const registerCategory = (values) => dispatch => {
  dispatch({type: REGISTER_REQUESTED});

  console.log('Registro de categoria requisitado...');

  axios.post("http://localhost:3001/categories", values).then(response => {
    dispatch(getAllCategories(true));
    dispatch({type: REGISTER_SUCCESS, error: null, category: values });
  }).catch(err => {
    dispatch({type: REGISTER_FAIL, error: err.response.data, category: values});
  }) 
  return Promise.resolve();
};

const removeCategory = (id) => dispatch => {

  dispatch({type: REMOVE_REQUESTED});

  console.log('Remoção de categoria requisitada...');

  axios.delete(`http://localhost:3001/categories/${id}`).then(response => {
    dispatch(getAllCategories(true));
    dispatch({type: REMOVE_SUCCESS, error: null});
  }).catch(err => {
    dispatch({type: REMOVE_FAIL, error: err.response.data });
  }) 
  return Promise.resolve();
};
      

const editCategory = (id, values) => dispatch => {
  dispatch({type: UPDATE_REQUESTED});

  console.log('Update de categoria requisitado...');

  axios.put(`http://localhost:3001/categories/${id}`, values).then(response => {
    dispatch(getAllCategories(true));
    dispatch({type: UPDATE_SUCCESS, success: true });
  }).catch(err => {
    dispatch({type: UPDATE_FAIL, error: err.response.data});
  })
  return Promise.resolve();
};

export default {
  getAllCategories,
  editCategory,
  registerCategory,
  removeCategory
};