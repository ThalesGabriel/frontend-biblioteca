import Router from 'next/router';
import {
  REGISTER_REQUESTED,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  UPDATE_REQUESTED,
  UPDATE_SUCCESS,
  UPDATE_FAIL,
  REMOVE_REQUESTED,
  REMOVE_SUCCESS,
  REMOVE_FAIL,
  USERS_REQUESTED,
  USERS_SUCCESS,
  USERS_FAIL,
} from '../types';
import { baseURL } from '../../../services/api';
import StringUtils from '../../StringUtils';
import axios from 'axios';

const getAllUsers = (returnToDataTable = false) => dispatch => {
  dispatch({type: USERS_REQUESTED});

  console.log('Relação de usuários requisitada...');

  axios.get("http://localhost:3001/users").then(response => {
    if(!returnToDataTable) dispatch({type: USERS_SUCCESS, users: response.data, columns: [], error: null });
    if(response.data.length > 0) {
      const columns = Object.keys(response.data[0])
      let columnsList = StringUtils.getColumnsToTableRecord(columns);
      columnsList = StringUtils.changeColumnName(columnsList, [null, "Nome", null, null])
      dispatch({type: USERS_SUCCESS, users: response.data, columns: columnsList });
    }
  }).catch(err => {
      dispatch({type: USERS_FAIL, error: err.response.data});
  })
  return Promise.resolve();
};

const registerUser = (values) => dispatch => {
  dispatch({type: REGISTER_REQUESTED});

  console.log('Registro de usuário requisitado...');

  axios.post("http://localhost:3001/signup", values).then(response => {
    dispatch(getAllUsers(true));
    dispatch({type: REGISTER_SUCCESS, error: null, user: values });
  }).catch(err => {
    dispatch({type: REGISTER_FAIL, error: err.response.data, user: values});
  }) 
  return Promise.resolve();
};

const removeUser = (id) => dispatch => {

  dispatch({type: REMOVE_REQUESTED});

  console.log('Remoção de usuário requisitada...');

  axios.delete(`http://localhost:3001/users/${id}`).then(response => {
    dispatch(getAllUsers(true));
    dispatch({type: REMOVE_SUCCESS, error: null});
  }).catch(err => {
    dispatch({type: REMOVE_FAIL, error: err.response.data });
  }) 
  return Promise.resolve();
};
      

const editUser = (id, values) => dispatch => {
  dispatch({type: UPDATE_REQUESTED});

  console.log('Update de usuário requisitado...');

  axios.put(`http://localhost:3001/users/${id}`, values).then(response => {
    dispatch(getAllUsers(true));
    dispatch({type: UPDATE_SUCCESS, success: true });
  }).catch(err => {
    dispatch({type: UPDATE_FAIL, error: err.response.data});
  })
  return Promise.resolve();
};

export default {
  getAllUsers,
  editUser,
  registerUser,
  removeUser
};
