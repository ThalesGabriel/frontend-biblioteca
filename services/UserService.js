import StringUtils from '../utils/StringUtils';
import axios from 'axios';

var UserService = (function(){

  return {
    getAllUsers: async function (returnToDataTable = false) { 
      try {
        const response = await axios.get("http://localhost:3001/users");
        if(!returnToDataTable) return response.data;
        if(response.data.length > 0) {
          const columns = Object.keys(response.data[0])
          let columnsList = StringUtils.getColumnsToTableRecord(columns);
          columnsList = StringUtils.changeColumnName(columnsList, [null, "Nome", null, null])
          return { users: response.data, columns: columnsList };
        }
      }catch(err) {
        return { error: err.response.data };
      }
    },

    editUser: function (id, values) {
      axios.put(`http://localhost:3001/users/${id}`, values).then(response => {
        return {type: UPDATE_SUCCESS, success: true };
      }).catch(err => {
        return {type: UPDATE_FAIL, error: {message: err}};
      })
    },

    deleteColumn: function (columns, removeColumns) { 
      for (let i = 0; i < removeColumns.length; i++) {
        if(removeColumns[i] != null && columns[i]["title"] == removeColumns[i]) {
          columns.splice(i,1);
        }
      }
      return columns;
    },

    capitalize: function (s) {
      if (typeof s !== 'string') return ''
      return s.charAt(0).toUpperCase() + s.slice(1)
    },
     
  }
 
 }())

module.exports = UserService;