import React from 'react'
import { Box, Typography } from '@material-ui/core'

export default function Title({label, sub, Icon}) {
  return (
    <>
      <Box style={{display: 'inline-flex'}}>
        <Icon style={{fontSize: '3rem'}}/>
        <Typography variant="h4" component="h4" style={{margin: 0, marginTop: 'auto', marginLeft: 10}}>{label}</Typography>
      </Box>
      <Typography style={{marginTop: 0, marginLeft: 7}}>{sub}</Typography>
      <hr style={{color: '#999'}}/>
    </>
  );
}