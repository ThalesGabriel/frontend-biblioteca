import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Autocomplete from '@material-ui/lab/Autocomplete';


const CustomInput = styled(TextField)`
  margin: 0;
  div.MuiInputBase-root {
    border-radius: 100px;
    background-color: white;
  }
  .custom-icon {
    margin-right: -40px;
  }
`;

export default function ComboBox(props) {
  const [ data, setData ] = useState(props.data || [])
  const [value, setValue] = useState(true)
  const [ id, setId ] = useState(props.rowData? props.rowData.id : 0)
  const {
    error,
    submitError,
    touched
  } = props;
  const handleParent = (value) => {
    props.handleFather(value)
    setValue(false)
  }

  function checkSame(obj) {
    return obj.id != id;
  }

  useEffect(() => {
    if(data.length > 0 && id != 0) {
      setData(data.filter(checkSame))
    }
  }, [id])

  return (
    
    <Autocomplete
      id="combo-box-demo"
      options={data}
      onChange={(event, value) => handleParent(value)}
      getOptionLabel={option => option[props.option]}
      renderInput={params => (
        <CustomInput 
          {...params} 
          {...props}
          label={props.label} 
          variant="outlined" 
          fullWidth 
          error={(error || submitError) && touched}
          helperText={(submitError || error) && touched ? (submitError|| error) : '' }/>
      )}
    />
  );
}