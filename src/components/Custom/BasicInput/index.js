import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import styled from 'styled-components';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';

import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const CustomTextField = styled(TextField)`
  div {
    border-radius: ${props => props.valueBorder || '200px'} ;
    background-color: white;
    width: 100%;
  }
  label {
    padding-top: ${props => props.lpt}
  }
  padding-bottom: ${props => props.pb || '20px'};
  padding-top: ${props => props.pt};
  display: block;
`

class BasicInput extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const {
      error,
      submitError,
      touched
    } = this.props;

    return (
      <CustomTextField
        type={'text'}
        variant="outlined"
        {...this.props}
        error={(error || submitError) && touched}
        helperText={(submitError || error) && touched ? (submitError|| error) : '' }

      />
    );

  }
}

BasicInput.defaultProps = {
  // iconName: null,
  // title: null,
  // subtitle: null,
  // ctaLabel: null,
  // onPressCta: () => {},
  // containerStyle: {}
};

BasicInput.propTypes = {
  // iconName: PropTypes.string,
  // title: PropTypes.string,
  // subtitle: PropTypes.string,
  // ctaLabel: PropTypes.string,
  // onPressCta: PropTypes.func,
  // containerStyle: PropTypes.any,
};


export default BasicInput;
