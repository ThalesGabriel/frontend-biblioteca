import React, {Component} from 'React'


import { makeStyles } from '@material-ui/core/styles';
import { Box, TextField } from '@material-ui/core';
import styled from 'styled-components';
import { connect } from 'react-redux';
import categoryActions from '../../../../utils/redux/actions/categoryActions';
import axios from 'axios';
import { compose } from 'recompose';
import actions from '../../../../utils/redux/actions';

import { withSnackbar } from '../../SnackbarProvider';

import CategoryForm from './CategoryForm';
import TableRecord from '../TableRecord';
import StringUtils from '../../../../utils/StringUtils';
import Modal from '../../Modal';
import { ModalChildrenEdit, ModalChildrenDelete } from './ModalChildren';

class Categories extends Component {

  constructor(props) {
    super(props);
    this.state = { del: false, rowData: {}, edit: false, parentObject: {} }
  }

  //Lifecycle
  componentDidMount() {
    this.props.getAllCategories(true);
  }

  componentDidUpdate(prevProps){
    const { success, error,  categories } = this.props;
    if (!prevProps.success && success){
      this.props.snackbar.showSuccessMessage();
    }
    if (prevProps.categories != categories){
      this.props.snackbar.showSuccessMessage();
    }
    if (!prevProps.error && error){
      this.props.snackbar.showFailureMessage(error);
    } 
  }

  handleRegister = (categoria) => {
    if(this.state.parentObject) {
      categoria.parentId = this.state.parentObject.id
    }else {
      categoria.parentId = null
    }
    this.props.registerCategory(categoria)
  }

  handleParent = (value) => {
    this.setState({parentObject: value})
  }

  handleClose = async () => {
    this.setState({edit: false, del: false, rowData: {}})
  }

  handleModalDeleteOpen(rowData) {
    this.setState({del: true})
    if(rowData != null) {
      this.setState({rowData: rowData})
    }
  };

  handleModalEditOpen(rowData) {
    this.setState({edit: true})
    if(rowData != null) {
      this.setState({rowData: rowData})
    }
  }

  handleDelete = () => {
    this.props.removeCategory(this.state.rowData.id)
    this.handleClose()
  }

  handleEdit = (values) => {
    if(this.state.parentObject != null) values.parentId = this.state.parentObject.id
    this.props.editCategory(this.state.rowData.id, values);
    this.handleClose()
  }

  handleClickDelete = (response = {}) => {
    this.setState({delete: !this.state.delete});
    if(response.status != null && Number.isInteger(response.status)) {
      if(response.status == 204){
        this.props.snackbar.showSuccessMessage('Categoria deletada com sucesso.');
        this.getCategories()
      }else {
        this.props.snackbar.showFailureMessage(response.data);
      }
    }
  };


  render() {

    return (
      <Box>
        <CategoryForm categories={this.props.categories} handleClick={this.handleClick} handleParent={this.handleParent} handleRegister={this.handleRegister}/>
        <Box style={{margin: 30}}/>
        <TableRecord 
          title="Categorias cadastradas" 
          search={"Procurar uma categoria"} 
          data={this.props.categories} 
          columns={this.props.columns} 
          handleModalEditOpen={this.handleModalEditOpen.bind(this)} 
          handleModalDeleteOpen={this.handleModalDeleteOpen.bind(this)}/>
        <Modal
          open={this.state.edit}
          handleClose={this.handleClose}
          title='Editar categoria'
          submit={null}
          >
            <ModalChildrenEdit rowData={this.state.rowData} data={this.props.categories} handleParent={this.handleParent} handleClose={this.handleClose} handleEdit={this.handleEdit}/>
        </Modal>
        <Modal
          open={this.state.del}
          handleClose={this.handleClose}
          title='Excluir categoria'
          submit={null}
          >
            <ModalChildrenDelete rowData={this.state.rowData} handleClose={this.handleClose} handleDelete={this.handleDelete}/>
        </Modal>
      </Box>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    categories: state.categoryReducer.categories,
    columns: state.categoryReducer.columns
  }
}


export default compose(connect(mapStateToProps, categoryActions), withSnackbar())(Categories);
