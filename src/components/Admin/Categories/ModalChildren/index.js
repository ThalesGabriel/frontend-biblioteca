import React, { useEffect, useState } from 'react'
import { Box, TextField, Typography, Button, Grid } from '@material-ui/core';
import styled from 'styled-components';

import { Form, Field } from 'react-final-form'
import { FORM_ERROR } from 'final-form'

import BasicAutocomplete from '../../../Custom/BasicAutocomplete';
import BasicInput from '../../../Custom/BasicInput';
import actions from '../../../../../utils/redux/actions';
import { required, email, password } from '../../../../../utils/validations';

const CustomGrid = styled(Grid)`
  .buttonDisabled {
    background-color: red;
  }
  .buttonRoot {
    margin: auto;
    width: 20%;
  }
  .buttonRight {
    float: right;
  }
`

export const ModalChildrenEdit = (props) => {
  const [ nome, setNome ] = useState("")

  useEffect(() => {
    if(props.rowData) {
      setNome(props.rowData.name)
    }
  }, [])

  const handleFather = (categoria) => {
    props.handleParent(categoria)
  }

  const onSubmit = async values => {
    props.handleEdit(values)
  }

  return (
    <Form
        onSubmit={onSubmit}
        initialValues={{name: nome || ""}}
        render={({handleSubmit, form, submitting, pristine, values, errors}) => {
          return (
            <form
            noValidate
            autoComplete="off"
            onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Field name="name" validate={required}>
                  {({ input, meta }) => (
                    <BasicInput {...input} {...meta} id="nome" label="Nome" placeholder="Informe o nome da categoria" variant="outlined" style={{width: '100%'}}/>
                  )}
                </Field>
              </Grid>
              <Grid item xs={12}>
                <Field name="parentId" >
                  {({ input, meta }) => (
                    <BasicAutocomplete {...input} {...meta} data={props.data} id="outlined-basic-father" rowData={props.rowData} label="Categoria pai"  handleFather={handleFather} variant="outlined" style={{width: '100%'}}/>
                  )}
                </Field>
              </Grid>
              <CustomGrid item xs={12} style={{display: 'flex'}}>
                <Button
                  variant="contained"
                  color="primary"
                  classes={{disabled: "buttonDisabled", root: "buttonRoot"}}
                  type="submit"
                >
                  Salvar
                </Button>
              </CustomGrid>
            </Grid>
          </form>
      )}}
    />    
  )
}

export const ModalChildrenDelete = (props) => {

  return (
    <Box>
      <Typography style={{textAlign: 'center'}}>Deseja apagar a categoria?</Typography>
      <Box style={{justifyContent: 'center', marginTop: 20, padding: 20}}>
        <Button variant="contained" color="primary" onClick={props.handleDelete} style={{height: 40, float: "left", width: "40%"}}>
          <Typography variant="body1" component="body1">Sim</Typography>
        </Button>

        <Button variant="contained" color="primary" onClick={props.handleClose} style={{height: 40, float: "right", width: '40%'}}>
          <Typography variant="body1" component="body1">Não</Typography>
        </Button>
      </Box>
    </Box>
  )
}