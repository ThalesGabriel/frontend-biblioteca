import React, {Component} from 'React'
import { connect } from 'react-redux';
import { compose } from 'recompose';
import axios from 'axios';

import { Typography, Grid, Paper, Checkbox, FormControlLabel, Button } from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles';
import SaveIcon from '@material-ui/icons/Save';
import DeleteIcon from '@material-ui/icons/Delete';
import styled from 'styled-components';

import { Form, Field } from 'react-final-form'
import { FORM_ERROR } from 'final-form'

import BasicInput from '../../../Custom/BasicInput';
import BasicAutocomplete from '../../../Custom/BasicAutocomplete';
import actions from '../../../../../utils/redux/actions';
import { required, email, password } from '../../../../../utils/validations';
import { withSnackbar } from '../../../SnackbarProvider';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  button: {
    margin: theme.spacing(1),
  },
  buttonDisabled: {
    backgroundColor: "red"
  }
}));

const CustomGrid = styled(Grid)`
  .buttonDisabled {
    background-color: red;
  }
  .buttonRight {
    float: right;
  }
`

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const onSubmit = async values => {
  await sleep(300)
  window.alert(JSON.stringify(values, 0, 2))
}

class CategoryForm extends Component {

  constructor(props) {
    super(props);
  }

  onSubmit = async values => {
    this.props.handleRegister(values)
  }

  render() {

    return (
      <Form
        onSubmit={this.onSubmit}
        render={({handleSubmit, form, submitting, pristine, values, errors}) => {
          return (
            <form
            noValidate
            autoComplete="off"
            onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <CustomGrid item xs={12}>
                  <Typography variant="h5" component="h5" style={{float: 'left'}}>Nova Categoria</Typography>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={form.reset}
                    disabled={submitting || pristine}
                    classes={{root: "buttonRight"}}
                    startIcon={<DeleteIcon />}
                  >
                    Limpar
                  </Button>
                </CustomGrid>
                <Grid item xs={12}>
                  <Field name="name" validate={required}>
                    {({ input, meta }) => (
                      <BasicInput {...input} {...meta} id="outlined-basic-nome" label="Nome"  placeholder="Informe o nome da categoria" variant="outlined" style={{width: '100%'}}/>
                    )}
                  </Field>
                </Grid>
                <Grid item xs={12}>
                  <Field name="parentId" >
                    {({ input, meta }) => (
                      <BasicAutocomplete {...input} {...meta} data={this.props.categories} option={"path"} id="outlined-basic-father" label="Categoria pai" handleFather={this.props.handleParent} variant="outlined" style={{width: '100%'}}/>
                    )}
                  </Field>
                </Grid>
                
                <CustomGrid item xs={12} style={{display: 'flex'}}>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={submitting || pristine }
                    classes={{disabled: "buttonDisabled"}}
                    type="submit"
                    startIcon={this.props.loading? null : <SaveIcon /> }
                  >
                    {this.props.loading? "Salvando" :"Salvar"}
                  </Button>
                </CustomGrid>

              </Grid>
            </form>
          )}}
        />
      );
    }
  }

const mapStateToProps = (state) => {
  return {
    loading: state.authentication.loading,
    error: state.authentication.error,
    user: state.authentication.user,
  }
}


export default compose(connect(mapStateToProps, actions), withSnackbar())(CategoryForm);
