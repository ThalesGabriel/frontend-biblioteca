import React, {Component} from 'React'

import { Box, TextField } from '@material-ui/core';
import { connect } from 'react-redux';
import userActions from '../../../../utils/redux/actions/userActions';
import { compose } from 'recompose';
import actions from '../../../../utils/redux/actions';

import UserForm from './UserForm';
import TableRecord from '../TableRecord';
import { withSnackbar } from '../../SnackbarProvider';
import Modal from '../../Modal';
import { ModalChildrenEdit, ModalChildrenDelete } from './ModalChildren';

class Users extends Component {

  constructor(props) {
    super(props);
    this.state = { delete: false, rowData: {}, edit: false }
  }

  //Lifecycle
  componentDidMount() {
    this.props.getAllUsers(true);
  }

  componentDidUpdate(prevProps){
    const { success, error,  users } = this.props;
    if (!prevProps.success && success){
      this.props.snackbar.showSuccessMessage();
    }
    if (prevProps.users != users){
      this.props.snackbar.showSuccessMessage();
    }
    if (!prevProps.error && error){
      this.props.snackbar.showFailureMessage(error);
    } 
  }

  //Redux
  handleRegister = (values) => {
    this.props.registerUser(values)
  }

  handleDelete = () => {
    this.props.removeUser(this.state.rowData.id)
    this.handleClose()
  }

  handleEdit = (values) => {
    this.props.editUser(this.state.rowData.id, values);
    this.handleClose()
  }

  //Modal
  handleClose = async () => {
    this.setState({edit: false, del: false, rowData: {}})
  }

  handleModalDeleteOpen(rowData) {
    this.setState({del: true})
    if(rowData != null) {
      this.setState({rowData: rowData})
    }
  };

  handleModalEditOpen(rowData) {
    this.setState({edit: true})
    if(rowData != null) {
      this.setState({rowData: rowData})
    }
  }
  
  //Snackbar
  showSnackbar = (msg) => {
    if(this.state.error) {
      this.props.snackbar.showFailureMessage(this.state.error);
    }else {
      this.props.snackbar.showSuccessMessage(msg);
    }
  }
  
  render() {

    return (
      <Box>
        <UserForm handleRegister={this.handleRegister}/>
        <Box style={{margin: 30}}/>
        <TableRecord 
          title="Usuários cadastrados" 
          search={"Procurar um usuário"} 
          data={this.props.users} 
          columns={this.props.columns} 
          handleModalEditOpen={this.handleModalEditOpen.bind(this)} 
          handleModalDeleteOpen={this.handleModalDeleteOpen.bind(this)}/>
        <Modal
          open={this.state.edit}
          handleClose={this.handleClose}
          title='Editar usuário'
          submit={null}
          >
            <ModalChildrenEdit rowData={this.state.rowData} handleClose={this.handleClose} handleEdit={this.handleEdit}/>
        </Modal>
        <Modal
          open={this.state.del}
          handleClose={this.handleClose}
          title='Excluir usuário'
          submit={null}
          >
            <ModalChildrenDelete rowData={this.state.rowData} handleClose={this.handleClose} handleDelete={this.handleDelete}/>
        </Modal>
      </Box>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    users: state.userReducer.users,
    columns: state.userReducer.columns
  }
}


export default compose(connect(mapStateToProps, userActions), withSnackbar())(Users);
