import React, {Component} from 'React'
import { connect } from 'react-redux';
import { compose } from 'recompose';

import { Typography, Grid, Paper, Checkbox, FormControlLabel, Button } from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles';
import SaveIcon from '@material-ui/icons/Save';
import DeleteIcon from '@material-ui/icons/Delete';
import styled from 'styled-components';

import { Form, Field } from 'react-final-form'
import { FORM_ERROR } from 'final-form'

import BasicInput from '../../../Custom/BasicInput';
import actions from '../../../../../utils/redux/actions';
import { required, email, password } from '../../../../../utils/validations';
import { withSnackbar } from '../../../SnackbarProvider';

const CustomGrid = styled(Grid)`
  .buttonDisabled {
    background-color: red;
  }
  .buttonRight {
    float: right;
  }
`

class UserForm extends Component {

  constructor(props) {
    super(props);
  }

  onSubmit = async values => {
    this.props.handleRegister(values)
  }

  render() {

    return (
      <Form
        onSubmit={this.onSubmit}
        validate={values => {
          const errors = {};
          if (this.props.error == 'Usuário já cadastrado' && this.props.user.email == values.email) {
            errors.email = 'Usuário já cadastrado';
          }
          if ((this.props.error == 'Senhas não conferem' || this.props.error == 'Confirmação de Senha inválida') && this.props.user.confirmPassword == values.confirmPassword) {
            errors.confirmPassword = this.props.error;
          }
          if(this.props.error == "Senha não informada") {
            errors.password = "Senha não informada";
          }
          if(this.props.error == "E-mail não informad") {
            errors.email = "E-mail não informad";
          }
          if(this.props.error == "Nome não informado") {
            errors.password = "Nome não informado";
          }
          return errors
        }}
        render={({handleSubmit, form, submitting, pristine, values, errors}) => {
          return (
            <form
            noValidate
            autoComplete="off"
            onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <CustomGrid item xs={12}>
                  <Typography variant="h5" component="h5" style={{float: 'left'}}>Novo Usuário</Typography>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={form.reset}
                    disabled={submitting || pristine}
                    classes={{root: "buttonRight"}}
                    startIcon={<DeleteIcon />}
                  >
                    Limpar
                  </Button>
                </CustomGrid>
                <Grid item md={6} xs={12}>
                  <Field name="name" validate={required}>
                    {({ input, meta }) => (
                      <BasicInput {...input} {...meta} id="outlined-basic-nome" label="Nome"  placeholder="Informe o nome de usuário" variant="outlined" style={{width: '100%'}}/>
                    )}
                  </Field>
                </Grid>
                <Grid item md={6} xs={12}>
                  <Field name="email" validate={email}>
                    {({ input, meta }) => (
                      <BasicInput {...input} {...meta} id="outlined-basic-email" label="Email" placeholder="Informe o email de usuário" variant="outlined" style={{width: '100%'}}/>
                    )}
                  </Field>
                </Grid>
                <Grid item xs={12}>
                  <FormControlLabel style={{paddingLeft: 5}} control={<Checkbox value="checkedD" color="primary" />} label="Administrador" />
                </Grid>
                <Grid item md={6} xs={12}>
                  <Field name="password" validate={password}>
                    {({ input, meta }) => (
                      <BasicInput {...input} {...meta} id="outlined-basic-senha" type="password" label="Senha" placeholder="Informe a senha do usuário" variant="outlined" style={{width: '100%'}}/>
                    )}
                  </Field>
                </Grid>
                <Grid item md={6} xs={12}>
                  <Field name="confirmPassword" validate={password}>
                    {({ input, meta }) => (
                      <BasicInput {...input} {...meta} id="outlined-basic-confirmar-senha" type="password" label="Confirmar senha" placeholder="Confirme a senha do usuário" variant="outlined" style={{width: '100%'}}/>
                    )}
                  </Field>
                </Grid>
                <CustomGrid item xs={12} style={{display: 'flex'}}>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={submitting || pristine}
                    classes={{disabled: "buttonDisabled"}}
                    type="submit"
                    startIcon={this.props.loading? null : <SaveIcon /> }
                  >
                    {this.props.loading? "Salvando" :"Salvar"}
                  </Button>
                </CustomGrid>

              </Grid>
            </form>
          )}}
        />
      );
    }
  }

const mapStateToProps = (state) => {
  return {
    loading: state.authentication.loading,
    error: state.userReducer.error,
    user: state.userReducer.user || {},
  }
}


export default compose(connect(mapStateToProps, actions), withSnackbar())(UserForm);
