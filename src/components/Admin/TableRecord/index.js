import React from 'react';
import MaterialTable from 'material-table';

export default function TableRecord(props) {

  return (
    <MaterialTable
      title={props.title}
      columns={props.columns}
      data={props.data}
      options={{
        actionsColumnIndex: -1
      }}
      localization={{
        header: {
          actions: 'Ações'
        },
        toolbar: {
          searchPlaceholder: props.search
        },
        body: {
          emptyDataSourceMessage: props.msg || 'Não há registros.',
          editRow: {
            deleteText: 'Apagar o Registro?',
          }
        }
      }}
      actions={[
        {
          icon: 'edit',
          tooltip: 'Editar',
          onClick: (event, rowData) => {
            props.handleModalEditOpen(rowData)
          }
        },
        {
          icon: 'delete',
          tooltip: 'Deletar',
          onClick: (event, rowData) => {
            props.handleModalDeleteOpen(rowData)
          }
        },
      ]}
    />
  );
}
