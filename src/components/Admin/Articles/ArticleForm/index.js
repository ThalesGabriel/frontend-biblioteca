import React, {Component} from 'React'
import { connect } from 'react-redux';
import { compose } from 'recompose';
import axios from 'axios';

import { Typography, Grid, Paper, Checkbox, FormControlLabel, Button, Box } from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles';
import SaveIcon from '@material-ui/icons/Save';
import DeleteIcon from '@material-ui/icons/Delete';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import styled from 'styled-components';

import { Form, Field } from 'react-final-form'
import { FORM_ERROR } from 'final-form'

import BasicInput from '../../../Custom/BasicInput';
import BasicAutocomplete from '../../../Custom/BasicAutocomplete';
import actions from '../../../../../utils/redux/actions';
import articleActions from '../../../../../utils/redux/actions/articleActions';
import { required, email, password } from '../../../../../utils/validations';
import { withSnackbar } from '../../../SnackbarProvider';
import SimpleMDE from "react-simplemde-editor";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  button: {
    margin: theme.spacing(1),
  },
  buttonDisabled: {
    backgroundColor: "red"
  }
}));

const CustomGrid = styled(Grid)`
  .buttonDisabled {
    background-color: red;
  }
  .root {
    margin-bottom: 30px;
  }
  .buttonRight {
    float: right;
  }
`

const Editor = styled(SimpleMDE) `
  div.editor-toolbar button.side-by-side {
    display: none;
  }
  div.editor-toolbar button.fullscreen {
    display: none;
  }

`

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const onSubmit = async values => {
  await sleep(300)
  window.alert(JSON.stringify(values, 0, 2))
}

class ArticleForm extends Component {

  constructor(props) {
    super(props);
    this.state = { selectedFile: null, authorObject: {}, categoryObject: {}}
  }

  onSubmit = async values => {
    if(this.state.categoryObject.id) values.categoryId = this.state.categoryObject.id;
    if(this.state.authorObject.id) values.userId = this.state.authorObject.id;
    this.props.registerArticle(values)
  }

  fileSelectedHandler = event => {
    this.setState({selectedFile:URL.createObjectURL(event.target.files[0])})
  }

  removeFile = event => {
    this.setState({selectedFile:null})
  }

  handleCategory = (value) => {
    this.setState({categoryObject: value || {}})
  }

  handleAuthor = (value) => {
    this.setState({authorObject: value || {}})
  }

  render() {

    return (
      <Form
        onSubmit={this.onSubmit}
        render={({handleSubmit, form, submitting, pristine, values, errors}) => {
          return (
            <form
            noValidate
            autoComplete="off"
            initialValue={{content: "Conteúdo"}}
            onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <CustomGrid item xs={12}>
                  <Typography variant="h5" component="h5" style={{float: 'left'}}>Novo Artigo</Typography>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={form.reset}
                    disabled={submitting || pristine}
                    classes={{root: "buttonRight"}}
                    startIcon={<DeleteIcon />}
                  >
                    Limpar
                  </Button>
                </CustomGrid>
                <Grid item xs={12}>
                  <Field name="name" validate={required}>
                    {({ input, meta }) => (
                      <BasicInput {...input} {...meta} id="outlined-basic-nome" label="Nome"  placeholder="Informe o nome do artigo" variant="outlined" style={{width: '100%'}}/>
                    )}
                  </Field>
                </Grid>
                <Grid item xs={12}>
                  <Field name="description" validate={required}>
                    {({ input, meta }) => (
                      <BasicInput {...input} {...meta} id="outlined-basic-email" label="Descrição"  placeholder="Informe a descrição do artigo" variant="outlined" style={{width: '100%'}}/>
                    )}  
                  </Field>
                </Grid>
                <Grid item xs={6}>
                  <Field name="imageUrl" validate={required}>
                    {({ input, meta }) => (
                      <BasicInput {...input} {...meta} id="outlined-basic-link" label="Imagem para upload"  placeholder="" variant="outlined" style={{width: '100%'}}/>
                    )}
                  </Field>
                </Grid>
                <Grid item xs={12}>
                  <Field name="categoryId">
                    {({ input, meta }) => (
                      <BasicAutocomplete {...input} {...meta} option={"path"} data={this.props.categories} id="outlined-basic-father" label="Categoria" handleFather={this.handleCategory} variant="outlined" style={{width: '100%'}}/>
                    )}
                  </Field>
                </Grid>
                <Grid item xs={12}>
                  <Field name="authorId">
                    {({ input, meta }) => (
                      <BasicAutocomplete input={input} meta={meta} option={"name"} data={this.props.users} id="outlined-basic-father" label="Autor" handleFather={this.handleAuthor} variant="outlined" style={{width: '100%'}}/>
                    )}
                  </Field>
                </Grid>
                <Grid item xs={12}>
                  <Field name="content" >
                    {({ input, meta }) => (
                      <>
                        <Editor {...input} {...meta} label="Conteúdo" initialValue="Conteúdo" style={{marginTop: 10, marginBottom: 0}} hideIcons={['preview', 'side-by-side']}/>
                      </>
                    )}
                  </Field>
                </Grid>
                
                <CustomGrid item xs={12} style={{display: 'flex'}}>
                  <Button
                    variant="contained"
                    color="primary"
                    disabled={submitting || pristine }
                    classes={{disabled: "buttonDisabled", root: "root"}}
                    type="submit"
                    startIcon={this.props.loading? null : <SaveIcon /> }
                  >
                    {this.props.loading? "Salvando" :"Salvar"}
                  </Button>
                </CustomGrid>

              </Grid>
            </form>
          )}}
        />
      );
    }
  }

  const mapStateToProps = (state) => {
    return {
      categories: state.categoryReducer.categories,
      users: state.userReducer.users,
    }
  }
  
  
  export default compose(connect(mapStateToProps, articleActions), withSnackbar())(ArticleForm);
