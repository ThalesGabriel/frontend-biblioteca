import React, {Component} from 'React'

import { makeStyles } from '@material-ui/core/styles';
import { Box, TextField, Typography } from '@material-ui/core';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import ArticleForm from './ArticleForm';
import TableRecord from '../TableRecord';

import actions from '../../../../utils/redux/actions';

import { withSnackbar } from '../../SnackbarProvider';
import articleActions from '../../../../utils/redux/actions/articleActions';
import Modal from '../../Modal';
import SimpleMDE from "react-simplemde-editor";

const Editor = styled(SimpleMDE) `
  box-shadow: 3px 3px 5px 1px #ccc;
  border-radius: 10px;
  div.editor-toolbar {
    padding: 10px;
    background-color: white;
    margin-top: 20px;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
  }

  div.editor-toolbar button {
    background: none;
    border-radius: 5px;
    border: 1px solid white;
    padding: 5px;
    font-size: 15px;
    margin: 1px;
  }

  div.editor-toolbar button:hover {
    cursor: pointer;
    background: none;
    border: 1px solid grey;
  }
`


class Articles extends Component {

  constructor(props) {
    super(props);
    this.state = { del: false, rowData: {}, edit: false, users: [], categories: [] }
  }

  //Lifecycle
  componentDidMount() {
    this.props.getAllArticles(true)
  }

  componentDidUpdate(prevProps){
    const { success, error } = this.props;
    console.log(this.props)
    if (!prevProps.success && success){
      this.props.snackbar.showSuccessMessage();
    } else if (!prevProps.error && error){
      this.props.snackbar.showFailureMessage(error);
    } else if(prevProps.time != this.props.time) {
      if(error) {
        this.props.snackbar.showFailureMessage(error);
      } else {
        this.props.snackbar.showSuccessMessage()
      }
		}
  }

  render() {

    return (
      <>
        <ArticleForm />
        
        <TableRecord 
          style={{marginTop: 20}}
          title="Artigos cadastrados" 
          search={"Procurar um artigo"} 
          data={this.props.articles || []} 
          columns={this.props.columns || []} 
        />
      </>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    categories: state.categoryReducer.categories,
    users: state.userReducer.users,
    articles: state.articleReducer.articles,
    columns: state.articleReducer.columns,
    success: state.articleReducer.success,
    error: state.articleReducer.error,
    time: state.articleReducer.time,
  }
}


export default compose(connect(mapStateToProps, actions), withSnackbar())(Articles);
