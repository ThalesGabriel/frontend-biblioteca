import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Typography, Avatar, IconButton, MenuItem, Menu } from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const useStyles = makeStyles(theme => ({
  name: {
    margin: 'auto',
    marginRight: 10,
  },
  avatar: {
    height: 48,
    width: 48,
  },
  menu: {
    marginTop: 50,
  }
  
}));

export default function Status({ user, handleClick }) {
  const classes = useStyles();
  const theme = useTheme();
  return(
    <>
      <Typography variant="body1" component="body1" className={clsx(classes.name)}>{user.name}</Typography>
      <Avatar className={clsx(classes.avatar)}>N</Avatar>
      <IconButton
        color="inherit"
        edge="end"
        aria-controls="simple-menu" 
        aria-haspopup="true" 
        onClick={handleClick}
        >
        <ArrowDropDownIcon />
      </IconButton>
    </>
  )
}