import React from 'react';
import Link from 'next/link';

import MenuIcon from '@material-ui/icons/Menu';
import { Typography, Box, IconButton, Toolbar, AppBar, Menu, MenuItem, Button, Hidden, useMediaQuery, Avatar, TextField } from '@material-ui/core';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';

import Status from './Status'
import Modal from '../../Modal'

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  menuButtonProfile: {
    display: 'inline-flex',
    marginLeft: 'auto',
  },
  centralizeTextToolbar: {
    marginLeft: 'auto',
  },
  hide: {
    display: 'none',
  },
  name: {
    margin: 'auto',
    marginRight: 10,
  },
  avatar: {
    height: 30,
    width: 30,
    cursor: 'pointer'
  },
  menu: {
    marginTop: 50,
  },
  loginButton: {
    color: 'white',
  },
  dividerLoginRegister: {
    margin: 'auto',
    backgroundColor: 'black',
    borderRadius: '50%',
    width: 30,
    height: 30,
    textAlign: 'center',
    alignItems: 'center',
    padding: 3
  },

}));


export default function Header({ open, handleDrawerOpen, toggleDrawer }) {
  const classes = useStyles();
  const theme = useTheme();
  const sm = useMediaQuery(theme.breakpoints.up('sm'));

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [user, setUser] = React.useState({});
  const [openModal, setOpenModal] = React.useState(false);

  const handleClickModal = () => {
    setOpenModal(!openModal);
  };

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={sm? toggleDrawer('left', true) : toggleDrawer('top', true)}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap className={clsx(classes.centralizeTextToolbar)}>
            <Link href="/"><a style={{textDecoration: 'none', color: 'white'}}>BRAINBASE - 2020</a></Link>
          </Typography>
          <Hidden xsDown>
            <Box className={clsx(classes.menuButtonProfile)}>
              {user.name?
                <Status user={user} handleClick={handleClick}/>
                :
                <Button href="#text-buttons" className={clsx(classes.loginButton)}  onClick={handleClickModal}>
                  Meu Acesso
                </Button>
              }
            </Box>
          </Hidden>
          <Hidden smUp>
            <Box className={clsx(classes.menuButtonProfile)}>
              {user.name?
                <Avatar className={clsx(classes.avatar)} onClick={handleClick}>N</Avatar>
                :
                null
              }
            </Box>
          </Hidden>
        </Toolbar>
      </AppBar>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        className={clsx(classes.menu)}
      >
        <MenuItem onClick={handleClose}><Link href="/admin"><a style={{textDecoration: 'none', color: '#111'}}>Administração</a></Link></MenuItem>
        <MenuItem onClick={handleClose}>Sair</MenuItem>
      </Menu>

      <Modal open={openModal} handleClick={handleClickModal} title="Login" comment="Para ter acesso a todas as funcionalidades do Brainbase faça seu login ou registre-se" submit="Submeter">
        <TextField
          autoFocus
          margin="dense"
          id="email"
          label="Seu email"
          type="email"
          fullWidth
        />
        <TextField
          autoFocus
          margin="dense"
          id="senha"
          label="Senha"
          type="password"
          fullWidth
        />
      </Modal>
    </>
  )
}
